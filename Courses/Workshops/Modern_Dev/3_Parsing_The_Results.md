# Take a look at the results of our pipeline running for the merge request, then merge the code to generate the security reports

## Theme

Following our shift left trend we will see how security results are included throughout each step of the deployment cycle.

* [ ] Step 1: Merge Request Security Results
  * Use the left hand navigation menu to click back into **Merge requests** and then click the merge request we created earlier.
  * Right below the approvals section we can see that our **_Code Quality, License Compliance, & Security scanning_** templates we included have generated full security reports unique to this branch for us to view. These reports are generated with each commit so we always know if we have introduced a new bug before its deployed out and disaster strikes.
  * Go ahead and take some time to expand each report and look through the results, then in the **_Security scanning_** section click on any of the critical/high vulnerabilities.
  * From this view we can see exactly where it occurred in the code & read up on why its a risk. After taking a look at this we can click the comment icon at the bottom to state that we checked it out and are not worried, then click **Add comment & dismiss**.

* [ ] Step 2: Coding With Code Suggestions
  * The merge request above only is only showing findings that already existed in our code. Lets go ahead and use GitLab Code Suggestions to write a new calculator class that we can add to our applciation. 
  * In the top right of our Merge Request view go ahead and click **Code > Open In Web IDE**.
  * First right click the lib/tanukiracing folder then right-click new file. Name this new file calc.rb.
  * We then want to add the prompt below to let Code Suggestions know what we are trying to write:

  ```plaintext
   #define a calculator class for other functions to use
  ```
  * Click enter and then wait a second for the suggestion to come in. As you are given suggestions, hit the TAB key to accept them. If it ever gets stuck try hitting the space bar or enter.
  * Code suggestions will write a very in depth calculator function and eventually will loop but feel free to stop it after 5 methods.
  * Code Suggestions dosent just work for ruby files either, and it supports multiple languages per project. Navigate into the **ai-sandbox/** folder for a list of currently up to date projects.
  * Choose one of the projects and test out code suggestions to write a hello world example or something more advanced. Your Instructor will give you time to do this now, but also keep in mind that you have access to the infra for another 48 hours to test what you want.
  * Now we want to commit this code to main. Go ahead and click the **Source Control** button on the left hand side and write a commit message. Next click **Commit & Push**.
  * Next on the resulting dropdown make sure you click commit to our mr branch, then on the popup click the **Go to merge request** button.
* [ ] Step 3: GitLab Duo in the Merge Request
  * Before merging we want to check out the various AI tools GitLab Duo provides within the merge request. Back in the Merge Request view expand the **Latest AI-generated summary** section to get a full write up that summarizes the changes our MR is bringing to main.
  * GitLab Duo will also generate unit test for you. To see the generated tests go ahead and click the **Changes** tab at the top of the Merge Request view.
  * Once on the changes tab locate our new calc.rb changes. On the file view click the three dots, then on the resulting drop down click **Suggest test cases** to see what unit tests you could add for this class.
  * Next we will want to scroll down and merge our code. At the bottom of the request click **Merge** to kick off our pipeline.
  * Once merged use the left hand navigation menu to click through **Build \> Pipelines** and click into the most recently kicked off pipeline. At this point you will go on a quick break until the pipeline completes.

* [ ] Step 4: Parsing the Results
  * Now that your **_main_** pipeline has completed the reports under **_Security & Compliance_** have been generated. These reports will only be generated if you run a pipeline against main.
  * Use the left hand navigation menu to click through **Secure-\> Security Dashboard**. This will bring up a dashboard view of all of the security vulnerabilities & their counts over time so you can track your work as you secure your project. This dashboard takes a long time to collect data so if yours still has no results your presenter will show you the dashboard of a deployed Tanuki Racing application [here](https://gitlab.com/gitlab-learn-labs/webinars/tanuki-racing/tanuki-racing-application/-/security/dashboard)
  * We have already seen how to view the vulnerabilities in the pipeline view, but now lets use the left hand navigation menu and click through **Secure -\> Vulnerability Report** to view the full report
  * First change the _All tools_ section under **Tools** to just filter on SAST. We can then click into any of the SAST vulnerabilities shown.
  * Inside the vulnerability we will want to click the _Try it out_ button within the **Explain this vulnerability** section. This will result in a popup appearing on the right hand side with some information on what the vulnerability is and how you can fix it. The Explain This Vulnerability feature currently works on any SAST vulnerabilities.
  * GitLab Duo also has the capability to explain any code in natural language. To see this you can also click the hyperlink in the **Location** section, then highlight a section of code.
  * Once the code is highlighted go ahead and click the **?** icon shown on the left hand side. This will then generate a code explianation for you. Please note this works best on full blocks of code.


* [ ] Step 5: Preventive Security Policies
  * We next want to change the filter for **Severity**  to _critical_ & change **Tool** to _Secret Detection_. We can click into any of the vulnerabilities present. We can see that one of our AWS tokens has already been leaked into the codebase.
  * To prevent this from ever happening in the future we can set up a new policy to run on all future merge requests. For our use case leaked tokens are easy mistakes that can lead to massive problems so we will create a quick policy to stop that. Use the left hand navigation menu to click through **Secure \> Policies** and then click **New policy**. On the resulting page click **Select policy** under **_Scan result policy_**.
  * Add a name to the policy, then under the **_Rules_** section we want to select **Security Scan** in the **When** dropdown list. Then we want to change **All scanners** to be **_Secret Detection_** and **All protected branches** to **default branch**.
  * Then under actions choose **individual users** as the **_Choose approver type_** and add **_lfstucker_** as the required approver and click **Configure with a merge request**. On the resulting merge request click ***merge*** and you will be brought to your new policy project that is applied to our workshop application. If you were to create another merge request with the leaked token still in the code based merging would be prevented until it was removed or you added your approval.
  * Before we move on lets go back to our project. Use the breadcrumbs at the top of the screen to click into your group, then once again click into your project.

> [Docs for policies](https://docs.gitlab.com/ee/user/application_security/policies/)

* [ ] Step 6: Take Action on Our Vulnerabilities
  * Now that we have a protective policy in place lets go ahead and ensure it works by removing the Secrets currently in the code base. From the main page our project lets go ahead and click **Web IDE** in the **Edit** dropdown list.
  * Click into the **_cf-sample-scripts/eks.yaml_** file and add our fake token **_aws_key_id AKIAIOSF0DNN7EXAMPLE_** at the end of the line 6. eg: Change the **description** from **_The name of the IAM role for the EKS service to assume._** to **The name of the IAM role for the EKS service to assume, using aws_key_id AKIAIOSF0DNN7EXAMPLE.**.
  * Once added click the source control button on the left hand side, add a quick commit message, then click **Commit & Push**
  * On the resulting drop down click **Yes** to open a new branch, then click the **_Enter_** key. A new popup will appear where we want to then click **Create MR**
  * Scroll to the bottom, uncheck **_Delete source branch when merge request is accepted_**, and click **Create merge request**
  * On the resulting MR notice that our policy requires approval from **_lfstucker_** before we are able to merge. In order for us to merge in the future we will have to remove the token and wait for the full pipeline to run.

> [Docs on automatically revoking secrets](https://docs.gitlab.com/ee/user/application_security/secret_detection/#responding-to-a-leaked-secret)
